package org.neocities.riverbanksquid.riverbankwisteria.item;

import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import org.neocities.riverbanksquid.riverbankwisteria.RiverbankWisteria;

// Create a custom item group, so our item appears in its own tab in the creative menu.
// The texture for the item passed to ItemStack() will be used as the icon shown on that tab.

public class ModItemGroup {
    public static final ItemGroup RB_WISTERIA = FabricItemGroupBuilder.build(new Identifier(RiverbankWisteria.MOD_ID, "rb_wisteria"), () -> new ItemStack(ModItems.WISTERIA_POD));
}
