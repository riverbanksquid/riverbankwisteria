package org.neocities.riverbanksquid.riverbankwisteria.item;

import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.FoodComponents;

// Create a custom FoodComponent to associate a status effect with the WISTERIA_POD item.
// When eaten, the pods will inflict a poison status effect equivalent to the vanilla pufferfish.

public class ModFoodComponents {
    public static final FoodComponent WISTERIA_POD = (new FoodComponent.Builder()).statusEffect(new StatusEffectInstance(StatusEffects.POISON, 1200), 1.0F).build();
}
