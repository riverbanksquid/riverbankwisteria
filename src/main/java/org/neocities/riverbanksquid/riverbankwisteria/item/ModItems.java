package org.neocities.riverbanksquid.riverbankwisteria.item;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.AliasedBlockItem;
import net.minecraft.item.Item;
import org.neocities.riverbanksquid.riverbankwisteria.RiverbankWisteria;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.neocities.riverbanksquid.riverbankwisteria.block.ModBlocks;

/*
* Create and register an AliasedBlockItem WISTERIA_POD to serve as our equivalent to glow berries.
* This must be an AliasedBlockItem instead of a BlockItem because the item's texture is different from the texture of
*  the block itself (the custom WISTERIA_VINES block).
*
* The item's name is "wisteria_pod", so all references to it in the "resources" directory must also use that name.
*
* Add the item to our custom ModItemGroup, so it appears in the creative menu.
*
* Add the custom WISTERIA_POD foodComponent to the item, so the game understands that it's edible & knows what should
*  happen if the player eats it.
* */

public class ModItems {
    public static final Item WISTERIA_POD = registerItem("wisteria_pod", new AliasedBlockItem(
            ModBlocks.WISTERIA_VINES, (new FabricItemSettings().group(ModItemGroup.RB_WISTERIA).food(ModFoodComponents.WISTERIA_POD))));

    private static Item registerItem(String name, Item item) {
        return Registry.register(Registry.ITEM, new Identifier(RiverbankWisteria.MOD_ID, name), item);
    }

    public static void registerModItems() {
        RiverbankWisteria.RB_WISTERIA_LOGGER.info("Riverbank Wisteria is registering items...");
    }

}
