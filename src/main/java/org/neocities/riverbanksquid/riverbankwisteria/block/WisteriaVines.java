package org.neocities.riverbanksquid.riverbankwisteria.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CaveVines;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.neocities.riverbanksquid.riverbankwisteria.item.ModItems;

// Extend vanilla CaveVines, giving the new interface a pickBerries method that causes it to drop our custom item when right-clicked.

public interface WisteriaVines extends CaveVines {
    static ActionResult pickBerries(BlockState state, World world, BlockPos pos) {
        if ((Boolean)state.get(BERRIES)) {
            Block.dropStack(world, pos, new ItemStack(ModItems.WISTERIA_POD, 1));
            float f = MathHelper.nextBetween(world.random, 0.8F, 1.2F);
            world.playSound((PlayerEntity)null, pos, SoundEvents.BLOCK_CAVE_VINES_PICK_BERRIES, SoundCategory.BLOCKS, 1.0F, f);
            world.setBlockState(pos, (BlockState)state.with(BERRIES, false), 2);
            return ActionResult.success(world.isClient);
        } else {
            return ActionResult.PASS;
        }
    }
}
