package org.neocities.riverbanksquid.riverbankwisteria.block;

import net.minecraft.block.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.neocities.riverbanksquid.riverbankwisteria.item.ModItems;

// Extend vanilla CaveVinesHeadBlock, overriding the methods that would otherwise point to vanilla blocks and items.
// Use our custom WisteriaVines interface for the return value of onUse.

public class WisteriaHeadBlock extends CaveVinesHeadBlock {

    public WisteriaHeadBlock(Settings settings) { super(settings); }

    protected Block getPlant() {
        return ModBlocks.WISTERIA_VINES_PLANT;
    }

    public ItemStack getPickStack(BlockView world, BlockPos pos, BlockState state) {
        return new ItemStack(ModItems.WISTERIA_POD);
    }

    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        return WisteriaVines.pickBerries(state, world, pos);
    }

}
