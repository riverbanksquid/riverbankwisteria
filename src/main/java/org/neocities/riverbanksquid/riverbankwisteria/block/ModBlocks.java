package org.neocities.riverbanksquid.riverbankwisteria.block;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.*;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.neocities.riverbanksquid.riverbankwisteria.RiverbankWisteria;
import org.neocities.riverbanksquid.riverbankwisteria.item.ModItemGroup;

/*
* Create a WisteriaHeadBlock WISTERIA_VINES and a WisteriaBodyBlock WISTERIA_VINES_PLANT by copying the settings of
* the vanilla CAVE_VINES and CAVE_VINES_PLANT blocks, respectively.
*
* Add only WISTERIA_VINES to our custom item group, since WISTERIA_VINES_PLANT isn't needed in the creative menu.
*
* No BlockItems should be created or registered, since WISTERIA_VINES will be represented by the item WISTERIA_POD.
*/

public class ModBlocks {
    public static final Block WISTERIA_VINES = registerBlock("wisteria_vines", new WisteriaHeadBlock(FabricBlockSettings.copyOf(Blocks.CAVE_VINES)), ModItemGroup.RB_WISTERIA);
    public static final Block WISTERIA_VINES_PLANT = registerBlock("wisteria_vines_plant", new WisteriaBodyBlock(FabricBlockSettings.copyOf(Blocks.CAVE_VINES_PLANT)), null);
            // is this anything

    private static Block registerBlock(String name, Block block, ItemGroup group) {
        //registerBlockItem(name, block, group);
        return Registry.register(Registry.BLOCK, new Identifier(RiverbankWisteria.MOD_ID, name), block);
    }
    //private static Item registerBlockItem(String name, Block block, ItemGroup group) {
    //    return Registry.register(Registry.ITEM, new Identifier(RiverbankWisteria.MOD_ID, name), new BlockItem(block, new FabricItemSettings().group(group)));
    //}
    public static void registerModBlocks() {
        RiverbankWisteria.RB_WISTERIA_LOGGER.info("Riverbank Wisteria is registering blocks...");
    }
}
