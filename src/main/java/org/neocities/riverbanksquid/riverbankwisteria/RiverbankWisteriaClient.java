package org.neocities.riverbanksquid.riverbankwisteria;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.client.render.RenderLayer;
import org.neocities.riverbanksquid.riverbankwisteria.block.ModBlocks;

// Give both of our ModBlocks transparency using RenderLayer.getCutout().

public class RiverbankWisteriaClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.WISTERIA_VINES, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.WISTERIA_VINES_PLANT, RenderLayer.getCutout());
    }
}
