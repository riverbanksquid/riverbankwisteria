package org.neocities.riverbanksquid.riverbankwisteria;

import net.fabricmc.api.ModInitializer;
import org.neocities.riverbanksquid.riverbankwisteria.block.ModBlocks;
import org.neocities.riverbanksquid.riverbankwisteria.item.ModItems;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Give our mod a MOD_ID and a logger.
// Use the mod's title in logging strings so the user can see which mod is printing to the console.

// Register our custom items and blocks on initialize.

public class RiverbankWisteria implements ModInitializer {
	public static final String MOD_ID = "riverbankwisteria";
	public static final Logger RB_WISTERIA_LOGGER = LoggerFactory.getLogger(MOD_ID);

	@Override
	public void onInitialize() {
		RB_WISTERIA_LOGGER.info("Riverbank Wisteria is adding wisteria vines...");
		ModItems.registerModItems();
		ModBlocks.registerModBlocks();
	}
}
