# Riverbank Wisteria

This mod uses textures created by Planet Minecraft user [pjzza3000](https://www.planetminecraft.com/member/pjzza3000/) for the resource 
pack [Glow Berry Vines to Wisteria!](https://www.planetminecraft.com/texture-pack/glow-berry-vines-to-wisteria/), with explicit permission.

Check out their excellent [glow lichen resource pack](https://www.planetminecraft.com/texture-pack/as-above-so-below-glow-lichen/), too!

With this mod, cave vines and wisteria can both be used, rather than the wisteria replacing the cave vines entirely.

The resource pack's "wisteria bud" item has been changed into a "wisteria pod" to match the seed pods of real wisteria plants.
The "wisteria bud" texture has been altered slightly to reflect this change without affecting the overall look of the vines.

## Obtaining Wisteria

Wisteria pods can be crafted by combining a glow berry with purple dye. The pods work just like glow berries and can 
be used to plant wisteria vines (or eaten, to no benefit and, in fact, moderate detriment!).

## This Repository

I've attempted to comment my code reasonably well, to make it easy to understand how the blocks and items were added.
I did this because learning to mod took me like 3 years & I think it would be cool if it took other people less time.
It's really easy to add plant blocks to Minecraft, even complicated ones that do stuff when you right-click on them 
like cave vines!

The "resources" directory can't be commentated as easily, but rest assured that it has the same structure as vanilla 
Minecraft's. (The JSON files there are also referenced heavily from vanilla files.)